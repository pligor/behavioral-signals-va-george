var io         = require('socket.io')();
var middleware = require('socketio-wildcard')();

io.use(middleware);

io.on('connection', function(socket) {
    console.log("connected!!");

    socket.on('*', function(packet){
        // client.emit('foo', 'bar', 'baz')
        // packet.data === ['foo', 'bar', 'baz']

        console.log('client emitted data and the packet is:');
        console.log(packet);
    });

    socket.on('disconnect', () => {
        console.log("disconnected...")
    });
});

const port = 5000;
io.listen(port);
console.log("listening on " + port);
