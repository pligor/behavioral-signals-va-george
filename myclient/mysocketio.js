const server = require('http').createServer();
const io = require('socket.io')(server);
io.on('connection', client => {
    client.on('event', data => {
        console.log("ok I have received an event with data:")
        console.log(data)
    });
    client.on('disconnect', () => {
        console.log("disconnected...")
    });
});
server.listen(3000);
console.log("listening on 3000");
