from __future__ import division

import re
import sys

from google.cloud import speech
from google.cloud.speech import enums
from google.cloud.speech import types
import pyaudio
from six.moves import queue
import os, io
import random

import threading
from time import sleep

# Audio recording parameters
RATE = 16000
CHUNK = int(RATE / 10)  # 100ms
#https://cloud.google.com/speech-to-text/docs/streaming-recognize

class AudioFileStream(object):
    # https://stackoverflow.com/questions/47961954/what-is-a-sample-format

    """Opens a recording stream as a generator yielding the audio chunks."""
    def __init__(self, rate, chunk, filename='helloworld_exit.wav'):
        #self._rate = rate
        self._chunk = chunk

        #paInt16 is 2 https://stackoverflow.com/questions/47961954/what-is-a-sample-format
        self.BYTE_CHUNK_SIZE = self._chunk * 2
        if filename[-3:] == 'ogg':
            self.BYTE_CHUNK_SIZE = self.BYTE_CHUNK_SIZE // 20
        print("final byte chunk size: {}".format(self.BYTE_CHUNK_SIZE))

        # Create a thread-safe buffer of audio data
        self._buff = queue.Queue()
        self.closed = True

        self._file_name = os.path.join(
            os.path.dirname(__file__),
            'resources',
            filename
        )

    def __enter__(self):
        #self._audio_interface = pyaudio.PyAudio()

        self._fp = io.open(self._file_name, 'rb')

        # self._audio_stream = self._audio_interface.open(
        #     format=pyaudio.paInt16,
        #     # The API currently only supports 1-channel (mono) audio
        #     # https://goo.gl/z757pE
        #     channels=1, rate=self._rate,
        #     input=True, frames_per_buffer=self._chunk,
        #     # Run the audio stream asynchronously to fill the buffer object.
        #     # This is necessary so that the input device's buffer doesn't
        #     # overflow while the calling thread makes network requests, etc.
        #     stream_callback=self._fill_buffer,
        # )

        self.closed = False

        self.thr = threading.Thread(target=self.provide_audio_file_in_chuncks,
                                    args=(), kwargs={})
        self.thr.start()

        return self

    def __exit__(self, type, value, traceback):
        self.thr.join()
        # self._audio_stream.stop_stream()
        # self._audio_stream.close()
        self.closed = True

        # Signal the generator to terminate so that the client's
        # streaming_recognize method will not block the process termination.
        self._buff.put(None)

        # self._audio_interface.terminate()
        self._fp.close()


    def provide_audio_file_in_chuncks(self):
        sleep(random.random()/10)  # sleep somewhere between 0 to 1 seconds
        in_data = self._fp.read(self.BYTE_CHUNK_SIZE)

        while len(in_data) > 0:
            sleep(random.random()/10)
            self._fill_buffer(in_data=in_data)
            in_data = self._fp.read(self.BYTE_CHUNK_SIZE)

        print("\nCURRENT SYSTEM: read the entire file !!\n")


    def _fill_buffer(self, in_data):
        """Continuously collect data from the audio stream, into the buffer."""
        # print(len(in_data))
        self._buff.put(in_data)
        # return None, pyaudio.paContinue

    def generator(self):
        while not self.closed:
            # Use a blocking get() to ensure there's at least one chunk of
            # data, and stop iteration if the chunk is None, indicating the
            # end of the audio stream.
            chunk = self._buff.get()
            if chunk is None:
                return
            data = [chunk]

            # Now consume whatever other data's still buffered.
            while True:
                try:
                    chunk = self._buff.get(block=False)
                    if chunk is None:
                        return
                    data.append(chunk)
                except queue.Empty:
                    break

            yield b''.join(data)


class MicrophoneStream(object):
    """Opens a recording stream as a generator yielding the audio chunks."""
    def __init__(self, rate, chunk):
        self._rate = rate
        self._chunk = chunk

        # Create a thread-safe buffer of audio data
        self._buff = queue.Queue()
        self.closed = True

    def __enter__(self):
        self._audio_interface = pyaudio.PyAudio()
        self._audio_stream = self._audio_interface.open(
            format=pyaudio.paInt16,
            # The API currently only supports 1-channel (mono) audio
            # https://goo.gl/z757pE
            channels=1, rate=self._rate,
            input=True, frames_per_buffer=self._chunk,
            # Run the audio stream asynchronously to fill the buffer object.
            # This is necessary so that the input device's buffer doesn't
            # overflow while the calling thread makes network requests, etc.
            stream_callback=self._fill_buffer,
        )

        self.closed = False

        return self

    def __exit__(self, type, value, traceback):
        self._audio_stream.stop_stream()
        self._audio_stream.close()
        self.closed = True
        # Signal the generator to terminate so that the client's
        # streaming_recognize method will not block the process termination.
        self._buff.put(None)
        self._audio_interface.terminate()

    def _fill_buffer(self, in_data, frame_count, time_info, status_flags):
        """Continuously collect data from the audio stream, into the buffer."""
        # print(len(in_data)) https://stackoverflow.com/questions/47961954/what-is-a-sample-format
        self._buff.put(in_data)
        return None, pyaudio.paContinue

    def generator(self):
        while not self.closed:
            # Use a blocking get() to ensure there's at least one chunk of
            # data, and stop iteration if the chunk is None, indicating the
            # end of the audio stream.
            chunk = self._buff.get()
            if chunk is None:
                return
            data = [chunk]

            # Now consume whatever other data's still buffered.
            while True:
                try:
                    chunk = self._buff.get(block=False)
                    if chunk is None:
                        return
                    data.append(chunk)
                except queue.Empty:
                    break

            yield b''.join(data)


def listen_print_loop(responses):
    """Iterates through server responses and prints them.

    The responses passed is a generator that will block until a response
    is provided by the server.

    Each response may contain multiple results, and each result may contain
    multiple alternatives; for details, see https://goo.gl/tjCPAU.  Here we
    print only the transcription for the top alternative of the top result.

    In this case, responses are provided for interim results as well. If the
    response is an interim one, print a line feed at the end of it, to allow
    the next result to overwrite it, until the response is a final one. For the
    final one, print a newline to preserve the finalized transcription.
    """
    #fp = open("output.txt", "w")
    num_chars_printed = 0
    #print("Listening Print Loop")
    for response in responses:
        print("RESPONSE_START")
        print(response)
        print("RESPONSE_END")
        #fp.write("{}\n".format(response))
        if not response.results:
            continue

        # The `results` list is consecutive. For streaming, we only care about
        # the first result being considered, since once it's `is_final`, it
        # moves on to considering the next utterance.
        result = response.results[0]
        if not result.alternatives:
            continue

        # Display the transcription of the top alternative.
        transcript = result.alternatives[0].transcript

        # Display interim results, but with a carriage return at the end of the
        # line, so subsequent lines will overwrite them.
        #
        # If the previous result was longer than this one, we need to print
        # some extra spaces to overwrite the previous result
        overwrite_chars = ' ' * (num_chars_printed - len(transcript))

        if not result.is_final:
            sys.stdout.write(transcript + overwrite_chars + '\r')
            sys.stdout.flush()

            num_chars_printed = len(transcript)
            #fp.writelines(['NOT FINAL'])
        else:
            print(transcript + overwrite_chars)
            #fp.writelines([transcript])

            # Exit recognition if any of the transcribed phrases could be
            # one of our keywords.
            if re.search(r'\b(exit|quit)\b', transcript, re.I):
                print('Exiting..')
                break

            num_chars_printed = 0

    #fp.close()

if __name__ == '__main__':
    # See http://g.co/cloud/speech/docs/languages
    # for a list of supported languages.
    language_code = 'en-US'  # a BCP-47 language tag

    client = speech.SpeechClient()
    config = types.RecognitionConfig(
        # encoding=enums.RecognitionConfig.AudioEncoding.LINEAR16,
        encoding= enums.RecognitionConfig.AudioEncoding.OGG_OPUS,
        sample_rate_hertz=RATE,
        language_code=language_code)
    streaming_config = types.StreamingRecognitionConfig(
        config=config,
        interim_results=True)

    # with MicrophoneStream(RATE, CHUNK) as stream:
    #     audio_generator = stream.generator()
    #     requests = (types.StreamingRecognizeRequest(audio_content=content)
    #                 for content in audio_generator)
    #
    #     responses = client.streaming_recognize(streaming_config, requests)
    #
    #     # Now, put the transcription responses to use.
    #     listen_print_loop(responses)

    # content = audio_file.read()
    # for ii in range(32):
    #     content = audio_file.read(3200)
    #     print(len(content))
    # audio = types.RecognitionAudio(content=content)

    #TODO MIND that the OGG file should NOT be Vorbis code but instead it should be OPUS
    with AudioFileStream(RATE, CHUNK, filename='helloworld_exit_opus.ogg') as stream:
        audio_generator = stream.generator()
        requests = (types.StreamingRecognizeRequest(audio_content=content)
                    for content in audio_generator)

        responses = client.streaming_recognize(streaming_config, requests)

        # Now, put the transcription responses to use.
        listen_print_loop(responses)

    # print(type(content))
    # print(len(content))
    # print(CHUNK)
