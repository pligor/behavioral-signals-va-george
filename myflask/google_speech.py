from __future__ import division

import re
import sys

from google.cloud import speech
from google.cloud.speech import enums
from google.cloud.speech import types
import pyaudio
from six.moves import queue
import os, io
import random

import threading
from time import sleep

from audio_stream import AudioStream
from tutorial_google_speech import listen_print_loop
from int16_to_byte_conv import int16_to_bytestring

class MySpeech(object):
    language_code = 'en-US'  # a BCP-47 language tag

    RATE = 16000
    CHUNK = int(RATE / 10)  # 100ms

    def __init__(self):
        self.client = speech.SpeechClient()
        config = types.RecognitionConfig(
            encoding=enums.RecognitionConfig.AudioEncoding.LINEAR16,
            sample_rate_hertz=self.RATE,
            language_code=self.language_code)
        self.streaming_config = types.StreamingRecognitionConfig(
            config=config,
            interim_results=True)

        self.audio_stream = AudioStream(self.RATE, self.CHUNK)

    def transcribing(self):
        self.audio_stream.enter()

        audio_generator = self.audio_stream.generator()
        requests = (types.StreamingRecognizeRequest(audio_content=content)
                    for content in audio_generator)

        responses = self.client.streaming_recognize(self.streaming_config, requests)

        # Now, put the transcription responses to use.
        # listen_print_loop(responses)
        self.thr = threading.Thread(target=listen_print_loop,
                                    args=(responses,), kwargs={})
        self.thr.start()

        #TODO self.audio_stream.exit() <--- put this line at the end of the function and/or at socket disconnect

    def speaking(self, int16_arr):
        byte_couples = [int16_to_bytestring(number) for number in int16_arr]
        bytestr = b''.join(byte_couples)
        #print("speaking {}".format(len(bytestr)))
        self.audio_stream.fill_buffer(bytestr)