#https://stackoverflow.com/questions/1239035/asynchronous-method-call-in-python
import threading
from time import sleep

class MyWorld(object):
    def __init__(self):
        self._state = "the past"

    def foo(self, param):
        sleep(3)
        print(self._state)
        print(param)

    def go(self):
        #important! it seems that really "self" is NOT required as a param
        thr = threading.Thread(target=self.foo, args=("my param",), kwargs={})
        thr.start()

        print("this should be printed first")

        sleep(0.1)
        self._state = 'the future'

        thr.join()


if __name__ == '__main__':
    MyWorld().go()
