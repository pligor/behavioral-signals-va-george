def int16_to_bytestring(number, debug=False):
    number += 2 ** 15
    assert number >= 0, "the number eventually should be positive"

    #TODO not sure of the convention which sound is first and which is second
    first_byte = number & 0x00FF
    second_byte = (number & 0xFF00) >> 8

    if debug == True:
        bin_repr = '{:08b}{:08b}'.format(second_byte, first_byte)
        print(bin_repr)
        assert int(bin_repr, 2) == number

    return bytes([first_byte, second_byte])

def two_bytes_to_int(byte_couple):
    return bytes(byte_couple)

if __name__ == '__main__':
    # aa = 40000  # 10011100 01000000
    aa = [(3, 4), (11, 23)]

    res = two_bytes_to_int(aa[0])
    print(type(res))
    print(res)

    def test_int16_to_bytestring():
        from random import randint

        aa = randint(0, (2 ** 16) - 1) - 2 ** 15
        print(aa)
        res = int16_to_bytestring(aa, True)
        print(res)
