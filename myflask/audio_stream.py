from __future__ import division

import re
import sys

from google.cloud import speech
from google.cloud.speech import enums
from google.cloud.speech import types
import pyaudio
from six.moves import queue
import os, io
import random

import threading
from time import sleep

class AudioStream(object):
    # https://stackoverflow.com/questions/47961954/what-is-a-sample-format

    """Opens a recording stream as a generator yielding the audio chunks."""
    def __init__(self, rate, chunk):
        #self._rate = rate
        self._chunk = chunk

        #paInt16 is 2 https://stackoverflow.com/questions/47961954/what-is-a-sample-format
        self.BYTE_CHUNK_SIZE = self._chunk * 2

        # Create a thread-safe buffer of audio data
        self._buff = queue.Queue()
        self.closed = True

    def enter(self):
        self.closed = False

    def exit(self):
        self.closed = True

        # Signal the generator to terminate so that the client's
        # streaming_recognize method will not block the process termination.
        self._buff.put(None)

    #Usage: self._fill_buffer(in_data=in_data)
    def fill_buffer(self, in_data):
        """Continuously collect data from the audio stream, into the buffer."""
        # print(len(in_data))
        self._buff.put(in_data)
        # return None, pyaudio.paContinue

    def generator(self):
        while not self.closed:
            # Use a blocking get() to ensure there's at least one chunk of
            # data, and stop iteration if the chunk is None, indicating the
            # end of the audio stream.
            chunk = self._buff.get()
            if chunk is None:
                return
            data = [chunk]

            # Now consume whatever other data's still buffered.
            while True:
                try:
                    chunk = self._buff.get(block=False)
                    if chunk is None:
                        return
                    data.append(chunk)
                except queue.Empty:
                    break

            joined_data = b''.join(data)

            yield joined_data