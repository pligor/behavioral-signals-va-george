#https://blog.miguelgrinberg.com/post/easy-websockets-with-flask-and-gevent
#https://github.com/miguelgrinberg/Flask-SocketIO
import random
from collections import OrderedDict
from google_speech import MySpeech
from flask import Flask, render_template
from flask_socketio import SocketIO, emit
import wave
from itertools import chain
app = Flask(__name__)
socketio = SocketIO(app)
import struct
myspeech = None
waveout = None
waveouts = OrderedDict()
mylist = ['AB', 'AC', 'AD', 'BA', 'BC', 'BD', 'CA', 'CB', 'CD', 'DA', 'DB', 'DC']
#Recall that WAV header is 44 bytes, the rest is raw data: http://www.topherlee.com/software/pcm-tut-wavformat.html

def init_waveout():
    #https://stackoverflow.com/questions/43881026/convert-32-bit-floating-points-to-16-bit-pcm-range
    global waveout
    waveout = _init_waveout()

def _init_waveout(filename= "output.wav"):
    fp = wave.open(filename, "wb")
    fp.setnchannels(1)
    fp.setsampwidth(2)  # which means 16 bit
    fp.setframerate(16000)
    fp.setnframes(0)
    fp.setcomptype('NONE', '')
    return fp

def int16_to_bytes(samples):
    # https://docs.python.org/2/library/struct.html
    # '<' means little endian, '>' means big-endian, count and "h" means short (2 byte)
    struct_format = "<{}h".format(len(samples))
    ints_as_bytes = struct.pack(struct_format, *samples)
    return ints_as_bytes

@app.route('/')
def index():
    return 'Hello, World!'

@app.route('/movie')
def movie():
    movies = ['godfather', 'deadpool', 'toy story', 'top gun', 'forrest gump']
    return random.choice(movies)

@socketio.on('my_event', namespace='/test')
def test_message(message):
    if isinstance(message, str) and message[0] == "{":
        import json
        message = json.loads(message)
    print(message)
    emit('my_response', {'data': 'client said: {}'.format(message['data'])})

@socketio.on('connect', namespace='/test')
def test_connect():
    print("TEST connected")
    emit('my_response', {'data': 'Server-Client Connected!'})

@socketio.on('disconnect', namespace='/test')
def test_disconnect():
    print("TEST DISconnected")
    emit('my_response', {'data': 'Server-Client Disconnected!'})

###############

@socketio.on('connect', namespace='/bs')
def client_connect():
    global myspeech
    myspeech = MySpeech()
    myspeech.transcribing()

    # https://stackoverflow.com/questions/43881026/convert-32-bit-floating-points-to-16-bit-pcm-range
    init_waveout()
    # for mycomb in mylist:
    #     waveouts[mycomb] = _init_waveout(filename= "output_{}.wav".format(mycomb))

    message = 'Server connected with client OK'
    print(message)

@socketio.on('sound_package', namespace='/bs')
def on_sound_package(int16_arr):
    print("sound package received")
    # print(message)
    # print(type(message))
    # print(len(message))

    # alphas = byte_arr[::4]
    # betas  = byte_arr[1::4]
    # ceas   = byte_arr[2::4]
    # deltas = byte_arr[3::4]
    #
    # #AB, AC, AD
    # #BA, BC, BD
    # #CA, CB, CD
    # #DA, DB, DC
    #
    # def process_byte_tuples(byte_tuples):
    #     return b''.join([bytes(byte_tuple) for byte_tuple in byte_tuples])
    #
    # thedict = OrderedDict()
    #
    # thedict['AB'] = process_byte_tuples(zip(alphas, betas))
    # thedict['AC'] = process_byte_tuples(zip(alphas, ceas))
    # thedict['AD'] = process_byte_tuples(zip(alphas, deltas))
    #
    # thedict['BA'] = process_byte_tuples(zip(betas, alphas))
    # thedict['BC'] = process_byte_tuples(zip(betas, ceas))
    # thedict['BD'] = process_byte_tuples(zip(betas, deltas))
    #
    # thedict['CA'] = process_byte_tuples(zip(ceas, alphas))
    # thedict['CB'] = process_byte_tuples(zip(ceas, betas))
    # thedict['CD'] = process_byte_tuples(zip(ceas, deltas))
    #
    # thedict['DA'] = process_byte_tuples(zip(deltas, alphas))
    # thedict['DB'] = process_byte_tuples(zip(deltas, betas))
    # thedict['DC'] = process_byte_tuples(zip(deltas, ceas))
    #
    # for mycomb in mylist:
    #     waveouts[mycomb].writeframesraw(thedict[mycomb])

    #the notion of a frame in 16bit wav file is the two-bytes (16 bit)
    assert all([-32768 <= nn <= 32767 for nn in int16_arr])
    global waveout
    waveout.writeframesraw(int16_to_bytes(int16_arr))

    global myspeech
    myspeech.speaking(int16_arr)

@socketio.on('disconnect', namespace='/bs')
def client_disconnect():
    global waveout
    waveout.close()
    # for mycomb in mylist:
    #     waveouts[mycomb].close()

    message = "Server DISCONNECTED from the client"
    print(message)

if __name__ == '__main__':
    app.run(host='127.0.0.1', port=5000, debug=True)

    def test_waveout():
        init_waveout()
        seconds = 5
        samples = [random.randint(-32000, 32000) for ii in range(seconds * 16000)]
        waveout.writeframes(int16_to_bytes(samples))
        waveout.close()